#ifndef MyQueue_H
#define MyQueue_H

template <typename T>
class MyQueue {
private:
     T *tab;
     int head, tail, msize;
public:
     MyQueue (int s): head(0), tail(0), msize(s) { tab = new T[s]; };
     ~MyQueue() { delete[] tab; };
     void push(T x);
     T pop(); 
     T& front();
     int size(); 
     bool empty();
     bool full();
     int mask(int);
     void clear();
     void display();
};

template <typename T> 
int MyQueue<T>::mask (int value) {
     return value & (msize - 1);
}

template <typename T>
void MyQueue<T>::push(T x) {
     tab[mask(tail++)] = x;
}

template<typename T>
void MyQueue<T>::clear(){
    if(!empty()){
        int sz = size();
        for(int i = 0; i<sz; i++){
            pop();
        }
    }
}
template <typename T>
T MyQueue<T>::pop() { 
     return tab[mask(head++)];
}

template <typename T>
void MyQueue<T>::display() { 
    std::cout << "Display:" << std::endl;
	std::cout << "[ ";
    for (int i = 0; i < size(); i++) {
        std::cout << tab[tail + i];
		if(i<size()-1) {
			std::cout<< ", ";
		}
    }
	std::cout << " ]" << std::endl;
}

template <typename T>
T& MyQueue<T>::front() { 
     return tab[mask(head)];
}

template <typename T> 
int MyQueue<T>::size() { 
     return tail-head;
}

template <typename T>
bool MyQueue<T>::empty() { 
     return size() == 0;
}

template <typename T>
bool MyQueue<T>::full() { 
     return size() == msize;
}

#endif
