#include <iostream>
#include <string>
#include "queue.h"

constexpr int n = 512;

using namespace std;

void help(){
     cout << "========================" << endl;
     cout << "F - push()" << endl;
     cout << "P - pop()" << endl;
     cout << "S - size()" << endl;
     cout << "C - clear()" << endl;
     cout << "D - display()" << endl;
     cout << "T - run test" << endl;
     cout << "Przykladowa komenda: f 1 --> dodajemy cyfre" << endl;
}

void test(){
     MyQueue<int> queue(n);
     cout << "==================" <<endl;
     cout << "Dodaje 1 do kolejki" <<endl;
     queue.push(1);
     cout << "Dodaje 2 do kolejki" <<endl;
     queue.push(2);
     cout << "Dodaje 3 do kolejki" <<endl;
     queue.push(3);
     cout << "Dodaje 4 do kolejki" <<endl;
     queue.push(4);
     queue.display();
     cout << "W kolejce znajduje sie " << queue.size() << " elementow" << endl;
     cout << "Usuwam " << queue.pop() << " z kolejki" << endl;
     cout << "Usuwam " << queue.pop() << " z kolejki" << endl;
     cout << "Usuwam " << queue.pop() << " z kolejki" << endl;
     cout << "Usuwam " << queue.pop() << " z kolejki" << endl;
     cout << "W kolejce znajduje sie " << queue.size() << " elementow" << endl;
     cout << "Dodaje 1 do kolejki" <<endl;
     queue.push(1);
     cout << "Dodaje 2 do kolejki" <<endl;
     queue.push(2);
     queue.display();
     cout << "Czyszczę kolejkę!" <<endl;
     queue.clear();
     cout << "W kolejce znajduje sie " << queue.size() << " elementow" << endl;
     cout << "==================" <<endl;

}
int main() {
     MyQueue<int> queue(n);
     string input;

     help();
    
     while(cin >> input) {
          char inputChar = toupper(input.at(0)); // case not sensitive
          switch(inputChar) {
               case 'F':
                    if(!queue.full()) {
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodaje " << x << " do kolejki" << endl;
                         queue.push(x);
                    } else {
                         cout << "Kolejka jest pelna!" << endl;
                    }
                    break;
               case 'S':
                    cout << "W kolejce znajduje sie " << queue.size() << " elementow" << endl;
                    break;
               case 'P':
                    if(!queue.empty()) {
                        cout << "Usuwam " << queue.pop() << " z kolejki" << endl;

                    } else {
                        cout << "Kolejka jest pusta!" << endl;
                    }
                    break;
               case 'C':
                    if(!queue.empty()) {
                        cout << "Czyszczę kolejkę!" <<endl;
                        queue.clear();

                    } else {
                        cout << "Kolejka jest pusta!" << endl;
                    }
                    break;
               case 'T':
                    test();
                    break;
                case 'H':
                    help();
                    break;
                case 'D':
                    if(!queue.empty()) {
                        queue.display();
                    } else {
                        cout << "Kolejka jest pusta!" << endl;
                    }
                    break;
               default:
                    break;
          }
     }

     return 0;
}