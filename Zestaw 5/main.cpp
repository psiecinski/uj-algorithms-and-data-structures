#include "doublelist.h"
#include <iostream>

void test() {
    DoubleList<int> list;
	list.push_back(1);
	list.push_back(2);
	list.display();
    list.push_front(12);
    list.push_front(1);
    list.display();
    list.size();

    list.push_front(13);
    list.push_front(12);
    list.push_back(11);
    list.display();
    list.push_back(11);
    list.display();
    list.pop_front();
    list.pop_back();
    list.display();
    if(!list.empty()) {
        std::cout << "Lista nie jest pusta " << std::endl;
    } else {
        std::cout << "Lista jest pusta!" << std::endl;
    }
    list.clear();
    list.display();
    if(!list.empty()) {
        std::cout << "Lista nie jest pusta " << std::endl;
    } else {
        std::cout << "Lista jest pusta!" << std::endl;
    }
}
int main() {
    test();
    return 0;
}
