#ifndef DOUBLELIST_H
#define DOUBLE_H

#include <iostream>

template<typename T>
class DoubleList {
private:
    struct DoubleNode;

public:
    DoubleList();
    ~DoubleList();

    bool empty();
    int size();
    void push_back(const T &);
    void push_front(const T &);
    void pop_back();
    void pop_front();
    void display();
    void clear();

    class Iterator;
    Iterator begin() const;
    Iterator end() const;

    class Iterator {
    public:
        Iterator(DoubleNode* node);
        DoubleNode* node() const;
        T& val() const;
        bool empty() const;
        Iterator& operator++();
        Iterator& operator--();
        bool operator==(const Iterator&) const;
        bool operator!=(const Iterator&) const;

    private:
        DoubleNode* Node;
    };

private:
    void AppendNode(const T&, const Iterator& position);
    void DeleteNode(const Iterator& position);

private:
    struct DoubleNode
    {
        DoubleNode *pPrev;
        DoubleNode *pNext;
        T value;
    };

    DoubleNode *HeadNode;
    DoubleNode *TailNode;

    int nodeSize;
};


template<typename T>
inline DoubleList<T>::Iterator::Iterator(DoubleNode* node):Node(node) {

}

template<typename T>
inline bool DoubleList<T>::Iterator::empty() const {
    return (Node != NULL);
}

template<typename T>
inline T& DoubleList<T>::Iterator::val() const {
    return (Node->value);
}

template<typename T>
typename DoubleList<T>::DoubleNode* DoubleList<T>::Iterator::node() const {
    return Node;
}

template<typename T>
typename DoubleList<T>::Iterator& DoubleList<T>::Iterator::operator++() {
    if (Node->pNext != NULL) {
        Node = Node->pNext;
    }
    return *this;
}

template<typename T>
typename DoubleList<T>::Iterator& DoubleList<T>::Iterator::operator--() {
    if (Node->pPrev != NULL) {
        Node = Node->pPrev;
    }
    return *this;
}

template<typename T>
inline bool DoubleList<T>::Iterator::operator==(const Iterator& plus) const {
    return (plus.Node == this->Node);
}

template<typename T>
inline bool DoubleList<T>::Iterator::operator!=(const Iterator& plus) const {
    return (plus.Node != this->Node);
}


template<typename T>
typename DoubleList<T>::Iterator DoubleList<T>::begin() const {
    return Iterator(HeadNode->pNext);
}

template<typename T>
typename DoubleList<T>::Iterator DoubleList<T>::end() const {
    return Iterator(TailNode);
}

template<typename T>
DoubleList<T>::DoubleList() {
    HeadNode = new DoubleNode();
    TailNode = new DoubleNode();
    HeadNode->pNext = TailNode;
    HeadNode->pPrev = NULL;
    TailNode->pNext = NULL;
    TailNode->pPrev = HeadNode;
    nodeSize = 0;
}

template<typename T>
DoubleList<T>::~DoubleList() {
    HeadNode->pNext = NULL;
    HeadNode->pPrev = NULL;
    TailNode->pNext = NULL;
    TailNode->pPrev = NULL;

    if (HeadNode) {
        delete HeadNode;
        HeadNode = NULL;
    }

    if (TailNode) {
        delete TailNode;
        TailNode = NULL;
    }

    nodeSize = 0;
}

template<typename T>
bool DoubleList<T>::empty() {
    if ((TailNode == HeadNode->pNext) && (HeadNode == TailNode->pPrev))
    {
        return true;
    }

    return false;
}

template<typename T>
int DoubleList<T>::size() {
    return nodeSize;
}

template<typename T>
void DoubleList<T>::clear() {
    nodeSize = 0;
    if (empty()) {
        return;
    } 
    for (Iterator item = this->begin(); item != this->end(); ++item) {
        DoubleNode* CurrentNode = item.node();
        delete CurrentNode;
        CurrentNode = NULL;
    }

    HeadNode->pNext = TailNode;
    TailNode->pPrev = HeadNode;
}

template<typename T>
void DoubleList<T>::push_back(const T& value) {
    Iterator iter(TailNode);
    AppendNode(value, iter);
}

template<typename T>
void DoubleList<T>::push_front(const T& value) {
    Iterator iter(HeadNode->pNext);
    AppendNode(value, iter);
}

template<typename T>
void DoubleList<T>::pop_back() {
    Iterator iter(TailNode->pPrev);
    DeleteNode(iter);
}

template<typename T>
void DoubleList<T>::pop_front() {
    Iterator iter(HeadNode->pNext);
    DeleteNode(iter);
}

template<typename T>
void DoubleList<T>::display() {
	int i = 0;
    std::cout << "Display:" << std::endl;
	std::cout << "[ ";
    for (Iterator iterator = this->begin(); iterator != this->end(); ++iterator) {
        std::cout << iterator.val();
		if(i<size()-1) {
			std::cout<< ", ";
		}
		i++;
    }
	std::cout << " ]" << std::endl;

}


template<typename T>
void DoubleList<T>::AppendNode(const T& value, const Iterator& position) {
    DoubleNode *NewNode = new DoubleNode();
    DoubleNode *CurrNode = position.node();
    NewNode->pNext = CurrNode;
    NewNode->pPrev = CurrNode->pPrev;
    CurrNode->pPrev = NewNode;
    NewNode->pPrev->pNext = NewNode;
    NewNode->value = value;

    ++nodeSize;
}

template<typename T>
void DoubleList<T>::DeleteNode(const Iterator& position) {
    DoubleNode *CurrNode = position.node();
    CurrNode->pNext->pPrev = CurrNode->pPrev;
    CurrNode->pPrev->pNext = CurrNode->pNext;
    delete CurrNode;
    CurrNode = NULL;
    --nodeSize;
}

#endif
