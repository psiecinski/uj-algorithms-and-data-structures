#ifndef MyStack_H
#define MyStack_H
#include <iostream>

template<typename T>

class MyStack {
private:
     int pos, n;
     T *tab;
public:
     MyStack(int size);
     ~MyStack();

     void push(T x); 
     T pop(); 
     T& top(); 
     int size(); 
     bool empty();
     bool full();
     void clear();    
     void display();    
};

template<typename T>
MyStack<T>::MyStack(int size): pos(0), n(size) {
     tab = new T[size];
}

template<typename T>
MyStack<T>::~MyStack() {
     delete[] tab;
}

template<typename T>
void MyStack<T>::push(T x) {
     if(!full()){
        tab[pos] = x;
        pos++; 
     } else {
         throw std::out_of_range("Za duzo elementow!");
     }
}

template<typename T>
T MyStack<T>::pop() { 
     if(!empty()){
        pos--;
        return tab[pos];  
     } else {
        throw std::out_of_range("Nie ma zadnych elementow!");
     }
}

template<typename T>
void MyStack<T>::clear(){
    if(!empty()){
        int sz = size();
        for(int i = 0; i<sz; i++){
            pop();
        }
    }
}

template<typename T>
bool MyStack<T>::full() {
     return (pos == n);
}

template<typename T>
void MyStack<T>::display() { 
     if(!empty()) {
        int sz = size();
        std::cout<<"[ ";
        for(int i = 0; i<sz; i++) {
            std::cout << tab[i];
            if(i<sz-1){
                std::cout<<", ";
            }
        }
        std::cout<<" ]" << std::endl;
     }
}

template<typename T>
T& MyStack<T>::top() {
     return tab[pos-1];
}


template<typename T>
bool MyStack<T>::empty() {
     return (pos == 0);
}

template<typename T>
int MyStack<T>::size() { 
     return pos;
}

#endif