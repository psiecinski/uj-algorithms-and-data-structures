#include "mystack.h"
#include <iostream>

int n = 3;

using namespace std;
void help(){
     cout << "========================" << endl;
     cout << "F - push()" << endl;
     cout << "P - pop()" << endl;
     cout << "S - size()" << endl;
     cout << "C - clear()" << endl;
     cout << "D - display()" << endl;
     cout << "Przykladowa komenda: f 1 --> dodajemy cyfre" << endl;
}
int main() {

     MyStack<int> stack(n);
     string input;
     
     help();

     while(cin >> input) {
          char inputChar = toupper(input.at(0)); // case not sensitive
          switch(inputChar) {
               case 'F':
                    if(!stack.full()) {
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodano do stosu: " << x << endl;
                         stack.push(x);
                    } else {
                         cout << "Stos jest pelny!" << endl;
                    }
                    break;
               case 'S':
                    cout << "W stosie jest aktualnie " << stack.size() << " elementow" << endl;
                    break;
               case 'P':
                    if(!stack.empty()) {
                         cout << "Usuwam " << stack.pop() << " ze stosu" << endl;
                    } else {
                         cout << "Nie ma elementow w stosie!" << endl;
                    }
                    break;
                case 'C':
                    if(!stack.empty()) {
                         stack.clear();
                         cout << "Usuwam caly stos" << endl;
                    } else {
                         cout << "Nie ma elementow w stosie!" << endl;
                    }
                    break;
                case 'D':
                    if(!stack.empty()) {
                         cout << "Oto stos: " << endl;
                         stack.display();
                    } else {
                         cout << "Nie ma elementow w stosie!" << endl;
                    }
                    break;
                case 'H':
                    help();
               default:
                    break;
          }
     }

     return 0;
}