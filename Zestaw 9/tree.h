#include <iostream>

using namespace std;

class RandomBinaryTree {
     public: 
     struct BSTNode {
          int key;
          BSTNode* left;
          BSTNode* right;
     };

     private:
     BSTNode* root;
     int nodesCount;
     int max(int a, int b);

     public:
     RandomBinaryTree();
     ~RandomBinaryTree();
     void insert(int x); // wstawia element do drzewa
     int calc_nodes(); // liczba wezlow
     int calc_nodes_recursive(BSTNode *n);
     int calc_height(BSTNode*);  // wysokosc drzewa
     void inorder(BSTNode*); // zawartosc wszystkich wezlow inorder
     void preorder(BSTNode*); 
     void postorder(BSTNode*); 
     void clear(BSTNode*); 
     void clear() { clear(root);}
     void display();
     BSTNode* getRoot();
};

     RandomBinaryTree::RandomBinaryTree() {
          nodesCount = 0;
          root = nullptr;
     }

     RandomBinaryTree::~RandomBinaryTree() {
          clear(root);
     }

     int RandomBinaryTree::max(int a, int b) {
          return (a>b ? a : b);
     }
     void RandomBinaryTree::clear(BSTNode* n) {
          if(n == nullptr) return;
          clear(n->left);
          clear(n->right);
          delete n;
          root = nullptr;
          nodesCount = 0;
     }

     void RandomBinaryTree::insert(int x) {
          if (root == nullptr) {
               root = new BSTNode;
               root->key = x;
               root->right = nullptr;
               root->left = nullptr;
               nodesCount++;
          } else {
               BSTNode* temp, *actualNode = root;
               while(actualNode != nullptr) {
                    temp = actualNode;
                    if (actualNode->key > x ) 
                         actualNode = actualNode->left;
                    else if (actualNode->key <= x) 
                         actualNode = actualNode->right;
               }
               actualNode = new BSTNode;
               actualNode->left = nullptr;
               actualNode->right = nullptr;
               actualNode->key = x;
               nodesCount++;
               if(temp->key > x) temp->left = actualNode;
               else temp->right = actualNode;
          }
     }           

     int RandomBinaryTree::calc_nodes() {
          return nodesCount;
     }
        
     int RandomBinaryTree::calc_nodes_recursive(BSTNode* n) {
          if(n == nullptr) return 0;
          else return 1 + calc_nodes_recursive(n->left) + calc_nodes_recursive(n->right);
     }

     int RandomBinaryTree::calc_height(BSTNode* n) {
          if(n == nullptr) return 0;
          int length_left = calc_height(n->left);
          int length_right = calc_height(n->right);
          return 1 + max(length_left, length_right);
     }

     void RandomBinaryTree::display() {
          if (nodesCount == 0) {
               cout << "No elements in array!" << endl;
               return;
          } else {
               BSTNode* stack[calc_nodes()];
               BSTNode* temp = root;
               int top = 0;
               while(temp != nullptr || top > 0) {
                    while(temp != nullptr) {
                         stack[top++] = temp;
                         temp = temp->left;
                    }
                    temp = stack[--top];
                    cout << temp->key << endl;
                    temp = temp->right;
               }
          }
     }

     void RandomBinaryTree::preorder(BSTNode *n) {
          cout << n->key << endl;
          if(n->left != nullptr) preorder(n->left);
          if(n->right != nullptr) preorder(n->right);
     }
     void RandomBinaryTree::postorder(BSTNode* n) {
          if(n->left != nullptr) postorder(n->left);
          if(n->right != nullptr) postorder(n->right);
          cout << n->key << endl;
     }

     void RandomBinaryTree::inorder(BSTNode* n) {
          if(root == nullptr) {
               return;
          } else {
               if(n->left != nullptr) inorder(n->left);
               cout << n->key << endl;
               if(n->right != nullptr) inorder(n->right);
          }

     }
     RandomBinaryTree::BSTNode* RandomBinaryTree::getRoot() {
          return root;
     }