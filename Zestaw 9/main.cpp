#include "tree.h"
#include <iostream>
#include <string>

int N = 3;

using namespace std;

int main() {
     RandomBinaryTree bt;
     int x;
     cout << "Podaj liczby: " << endl;
     for(int i = 0; i < N; i++) {
          cin >> x;
          bt.insert(x);
     }


     cout << "========" << endl;
     cout << "Display" << endl;
     cout << "========" << endl;
     bt.inorder(bt.getRoot());
     cout << "Root: " << bt.calc_nodes_recursive(bt.getRoot()) << endl;
     cout << "Wysokosc: " << bt.calc_height(bt.getRoot()) << endl;
     cout << "Wezly: " << bt.calc_nodes() << endl;
     cout << "Wezly Rekursja: " << bt.calc_nodes_recursive(bt.getRoot()) << endl;

     bt.clear();
     cout << endl;
     cout << "========" << endl;
     cout << "Display" << endl;
     cout << "========" << endl;
     bt.inorder(bt.getRoot());
     cout << "Wezly: " << bt.calc_nodes() << endl;
     cout << "Wezly Rekursja: " << bt.calc_nodes_recursive(bt.getRoot()) << endl;
     return 0;
}