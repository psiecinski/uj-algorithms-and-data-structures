#ifndef SINGLELIST_h
#define SINGLELIST_HXX

#include <iostream>
#include <cassert>

class SingleNode { 
     struct node {             
          node() { this->x = 0; };
          int x;                    
          int next;               
     };

private:
     node* arr;
     int head;     
     int size_;
     int free; 
     int n;

public: 
     SingleNode(int);
     ~SingleNode();
     void push_front(int); 
     int pop_front();
     void push_back(int);
     int pop_back();
     int find(int);
     int insert(int, int);
     int erase(int); 
     bool full();
     bool empty();
     int size();
     int getSize();
     void display();
     void clear();
     void reverse();
};

SingleNode::SingleNode(int size): head(-1), size_(size), free(0), n(0) {
     arr = new node[size];
     int i;
     for(i = 0; i < size-1; i++) {
          arr[i].x = 0;
          arr[i].next = i+1;
     }
     arr[i].x = 0;
     arr[i].next = -1;
}

SingleNode::~SingleNode() {
     delete arr;
}

void SingleNode::push_front(int x) { //
     if(!full()) {
          int nextFree = arr[free].next;

          arr[free].x = x;
          arr[free].next = head;
          head = free;
          free = nextFree;
          n++;
     }
}

int SingleNode::pop_front() {
     if(!empty()) {
          int Number = arr[head].x;
          arr[head].x = 0;
          int nextIndex = arr[head].next;
          int firstFree = free;

          free = head;
          arr[free].next = firstFree;
          head = nextIndex;
          n--;

          return Number;
     }
     return -1;
}

void SingleNode::push_back(int x) {
     if(!full()) {
          if(!empty()) {
               int nextFree = arr[free].next;
               arr[free].x = x;
               arr[free].next = -1;

               int i = head;
               while(arr[i].next != -1) {
                    i = arr[i].next;
               }

               arr[i].next = free;
               
               free = nextFree;
          } else {
               head = free;
               int nextFree = arr[free].next;
               arr[free].x = x;
               arr[free].next = -1;

               free = nextFree;
          }
          n++;
     }
}

int SingleNode::pop_back() {
     if(!empty()) {
          if(arr[head].next == -1) {
               int Number = arr[head].x;
               arr[head].x = 0;
               arr[head].next = free;
               free = head;
               head = -1;
               n = 0;
               return Number;
          } else {
               int firstFree = free;
               int i = arr[head].next, j = head;

               while(arr[i].next != -1) {
                    j = i;
                    i = arr[i].next;
               }
               arr[j].next = -1;

               free = i;
               arr[free].next = firstFree;
               int Number = arr[free].x;
               arr[free].x = 0;
               n--;
               return Number;
          }
     }
     return -1;
}

int SingleNode::find(int x) {
     if(!empty()) {
          for(int i = head; i != -1; i=arr[i].next) {
               if(arr[i].x == x) return i;
          }
     }
     return -1;
}

int SingleNode::insert(int pos, int x) {
     if(!full() && (pos < getSize() && pos >= 0)) {          
          if(head == pos) {
               int nextFree = arr[free].next;

               arr[free].x = x;
               arr[free].next = head;
               head = free;
               free = nextFree;
               n++;
               return head;
          } else {
               for(int i = head; i != -1; i = arr[i].next) {
                    if(arr[i].next == pos) {
                         int posOfNewElement = free;

                         int nextFree = arr[free].next;
                         arr[free].x = x;
                         arr[i].next = free;
                         arr[free].next = pos;



                         free = nextFree;
                         n++;
                         return posOfNewElement;
                    }
               }
          }
     }
     return -1;
}
void SingleNode::reverse()
{

}
int SingleNode::erase(int pos) {
     if(!empty() && (pos < getSize() && pos >= 0)) {
          if(head == pos) {
               int nextIndex = arr[head].next;

               arr[head].x = 0;
               arr[head].next = free;
               free = head;
               head = nextIndex;
               n--;
               return nextIndex;
          } else {
               for(int i = head; i != -1; i = arr[i].next) {
                    if(arr[i].next == pos) {
                         int nextIndex = arr[pos].next;
                         arr[i].next = nextIndex;

                         // zerowanie i przepinanie usuwanej komorki
                         arr[pos].x = 0;
                         arr[pos].next = free;

                         free = pos;
                         n--;
                         return nextIndex;
                    }
               }
          }
     }
     return -1;
}

bool SingleNode::full() {
     return (free == -1);
}

bool SingleNode::empty() {
     return (head == -1);
}

int SingleNode::getSize() {
     return size_;
}

int SingleNode::size() {
     return n;
}
void SingleNode::clear() {
     int i;
     do {
          for(i = head; i != -1; i = arr[i].next) {
               erase(find(arr[i].x));
          }
     } while(n>0);

}

void SingleNode::display() {
     std::cout << "[ ";
     for(int i = head; i != -1; i = arr[i].next) {
           std::cout << arr[i].x << " ";
     }
     std::cout << "]";
     std::cout << std::endl;
}


#endif