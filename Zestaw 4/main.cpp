#include <iostream>
#include <cctype>
#include <string>

#include "singlelist.h"
using namespace std;

void help(){
     cout << "========================" << endl;
     cout << "F - push_front()" << endl;
     cout << "B - push_back()" << endl;
     cout << "S - sprawdz jaki jest rozmiar" << endl;
     cout << "N - pop_front" << endl;
     cout << "M - pop_back" << endl;
     cout << "P - wyswietl liste" << endl;
     cout << "E - sprawdz, czy lista jest pusta" << endl;
     cout << "C - znajdz index wskazanej cyfry" << endl;
     cout << "x - komenda clear" << endl;
     cout << "h - help" << endl;
     cout << "Przykladowa komenda: f 1 --> dodajemy cyfre do przodu" << endl;
}

int main() {

    int size = 512;
    string input;
    SingleNode list(size);
    
    if(!system("clear"));
    help();

    while(cin >> input) {
        char inputChar = toupper(input.at(0)); // case not sensitive
        switch(inputChar) {
               case 'F':
                    if(!list.full()) {       
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodano element z przodu listy" << endl;
                         list.push_front(x);
                    } else {
                         cout << "Lista jest pelna!" << endl;
                    }
                    break;
               case 'B':
                    if(!list.full()) {
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodano element z tylu listy" << endl;
                         list.push_back(x);
                    } else {
                         cout << "Lista jest pelna!" << endl;
                    }
                    break;
               case 'S':
                    cout << "W liscie znajduje/a sie: " << list.size() << " element/y" << endl;
                    break;
                case 'P':
                         list.display();
                    break;
                case 'X':
                    if(!list.empty()) {
                         list.clear();
                    } else {
                         cout << "Lista jest pusta! Nie mozna jej wyczyscic!" << endl;
                    }
                    break;
                case 'E':
                    if(list.empty()){
                         cout << "Lista jest pusta!" << endl;
                    } else {
                         cout << "Lista nie jest pusta!" << endl;
                    }
                    break;  
                case 'N':
                    if(!list.empty()) {
                         cout << "Wyciagamy element z przodu listy: " << list.pop_front() << endl;
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;     
               case 'M':
                    if(!list.empty()) {
                         cout << "Usuwamy z tylu listy element: " << list.pop_back() << endl;
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;    

               case 'C':
                    if(!list.empty()) {
                         cin >> input;
                         int x = stoi(input);
                         if((list.find(x)) != -1) {
                              cout << "Znaleziono na pozycji: " << x << endl;
                         } else {
                              cout << "Nie znaleziono!" << endl;
                         }
                         
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;
                case 'R':
                    if(!list.empty()) {
                         list.reverse();
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                case 'H':
                    help();
                    break;
               default:
                    break;
          }
     }

     return 0;
     
}
