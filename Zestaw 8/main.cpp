#include <iostream>
#include <string>
#include "priority_queue.h"

using namespace std;
void help(){
     cout << "========================" << endl;
     cout << "F - push()" << endl;
     cout << "P - pop()" << endl;
     cout << "S - size()" << endl;
     cout << "C - clear()" << endl;
     cout << "D - display()" << endl;
     cout << "Przykladowa komenda: f 1 --> dodajemy cyfre" << endl;
}

int main() {
     Priority_Queue<int> queue;
     string input;

     help();

     while(cin >> input) {
          char inputChar = toupper(input.at(0)); // case not sensitive
          switch(inputChar) {
               case 'F':
                    if(!queue.full()) {
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodaje " << x << " do kolejki" << endl;
                         queue.push(x);
                    } else {
                         cout << "Kolejka jest pelna!" << endl;
                    }
                    break;
               case 'S':
                    cout << "W kolejce znajduje sie " << queue.size() << " elementow" << endl;
                    break;
               case 'P':
                    if(!queue.empty()) {
                        cout << "Usuwam z kolejki" << endl;
                        queue.pop();

                    } else {
                        cout << "Kolejka jest pusta!" << endl;
                    }
                    break;
               case 'C':
                    if(!queue.empty()) {
                        cout << "Czyszczę kolejkę!" <<endl;
                        queue.clear();

                    } else {
                        cout << "Kolejka jest pusta!" << endl;
                    }
                    break;  
                case 'H':
                    help();
                    break;
                case 'D':
                    if(!queue.empty()) {
                        queue.display();
                    } else {
                        cout << "Kolejka jest pusta!" << endl;
                    }
                    break;
               default:
                    break;
          }
     }
    return 0;
}