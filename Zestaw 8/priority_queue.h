#ifndef PriorityQueue_H
#define PriorityQueue_H

#define MAX_N 1000001
template <typename T>

class Priority_Queue
{
  private:
    T PQ[MAX_N];
    int heap_size;

  public:
    Priority_Queue() {
        heap_size = 0;
    }
    bool empty() {
        return (heap_size <= 0);
    }
    int size() {
        return heap_size;
    }
    bool full() {
        return size() == MAX_N;
    }
    void clear(){
        if(!empty()){
            int sz = size();
            for(int i = 0; i<sz; i++){
                pop();
            }
        }
    }
    int getMinimum() {
        if (!empty()) {
            return PQ[1];
        }
    }
    void display() {
        if (!empty()) {
            std::cout << "Display:" << std::endl;
	        std::cout << "[ ";
            for(int i = 1; i < heap_size + 1; i++) {
                std::cout << PQ[i];
		        if(i<heap_size) {
			        std::cout<< ", ";
		        }
            }
	        std::cout << " ]" << std::endl;
        } 
    }
    void push(T x) {
        PQ[++heap_size] = x;
        int pos = heap_size;
        while (pos > 1 && PQ[pos / 2] > PQ[pos]) {
            std::swap(PQ[pos / 2], PQ[pos]);
            pos /= 2;
        }s
    }

    void pop() {
        int ret, left, right, pos = 1;  
        std::swap(PQ[pos], PQ[heap_size--]);
        while (pos <= heap_size) {
            ret = pos;
            left = pos * 2;
            right = pos * 2 + 1;
            if (left <= heap_size && PQ[left] < PQ[ret])
                ret = left;
            if (right <= heap_size && PQ[right] < PQ[ret])
                ret = right;
            if (ret != pos) {
                std::swap(PQ[pos], PQ[ret]);
                pos = ret;
            }
            else
                break;
        }
    }
};

#endif