// Zadanie 2.3
#include <iostream>
#include "bubblesort.h"

using namespace std;
int main () {
    
    int numbers[] = {99, 522, 77, 8, 32, 232, 19, 19};
    const int length = sizeof(numbers) / sizeof(int);
    
    cout << "Przed sortowaniem: ";
    for (int i = 0; i < length; ++i) {
        cout << numbers[i] << " ";
    }
    cout << std::endl;
    bubblesort(numbers, length);
    
    cout << "Po sortowaniu: ";
    for (int i = 0; i < length; ++i) {
        cout << numbers[i] << " ";
    }
    cout << endl;

    return 0;
}