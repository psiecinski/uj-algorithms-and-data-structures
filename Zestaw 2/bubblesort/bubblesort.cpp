#include <algorithm>
#include "bubblesort.h"

void bubblesort(int *array, int length) {
    if (!array || (length < 2))
        return;
        
        for (int i = 0; i < length; ++i) {
            for (int j = 0; j < length - 1; ++j) {
                if (array[j] > array[j+1]) {
                    std::swap(array[j], array[j+1]);
                }
            }
        }
}