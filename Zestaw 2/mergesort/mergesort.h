#ifndef Mergesort_H
#define Mergesort_H
int merge(int *arr, int first, int middle, int last);
int mergeSortFunc(int *arr, int first, int last);
#endif 
