int mergeFunc(int *array, int first, int middle, int last) {

	int i, j, k, n, m;
	n = middle - first + 1;
	m = last - middle;
	int left[n], right[m];
	k = first;
	i = 0;
	j = 0;

	for (int i = 0; i < n; i++) {
		left[i] = array[first + i];
	}
	for (int j = 0; j < m; j++) {
		right[j] = array[middle + j + 1];
	}

	while (i < n && j < m) {
		if (left[i] < right[j]) {
			array[k] = left[i];
			i++;
		}
		else {
			array[k] = right[j];
			j++;
		}
		k++;
	}

	while (i < n) {
		array[k] = left[i];
		i++;
		k++;
	}

	while (j < m) {
		array[k] = right[j];
		j++;
		k++;
	}
	return 0;
}

int mergeSortFunc(int *array, int first, int last) {
	int middle;
	if (first < last) {
		middle = (first + last) / 2;
		mergeSortFunc(array, first, middle);
		mergeSortFunc(array, middle + 1, last);
		mergeFunc(array, first, middle, last);
	}
	return 0;
}