//zadanie 2.5
#include <iostream>
#include "mergesort.h"

using namespace std;

int main() {
  int numbers[] = {99, 522, 77, 8, 32, 232, 19, 19};
	int length = sizeof(numbers) / sizeof(int);

  cout << "Przed sortowaniem: ";
    for (int i = 0; i < length; i++) {
        cout << numbers[i] << " ";
    }
  cout << std::endl;
	mergeSortFunc(numbers, 0, length-1);

  cout << "Po sortowaniu: ";
    for (int i = 0; i < length; i++) {
        cout << numbers[i] << " ";
    }
  cout << endl;

	return 0;
}