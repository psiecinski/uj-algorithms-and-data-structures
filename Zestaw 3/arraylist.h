#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include <bits/stdc++.h> 
#include <iostream>
#include <algorithm> 
#include <cassert>
#include <list>

template<typename T>
class ArrayList { 
private:
     T* tab;
     int last, msize;

public:
     ArrayList(int);
     ~ArrayList();
     int find(T);
     int insert(int, T);
     int erase(int);
     int front();
     int back();
     void push_front(T);
     void push_back(T);
     void display();
     void clear();
     void reverse();
     void sort();
     T pop_front();
     T pop_back();
     bool full();
     bool empty();
     int size();
};

template<typename T>
ArrayList<T>::ArrayList(int size): last(0), msize(size) {
     tab = new T[size];
}

template<typename T>
ArrayList<T>::~ArrayList() {
     delete[] tab;
}

template<typename T>
void ArrayList<T>::push_front(T x) {
     tab[last++] = x;
}

template<typename T>
T ArrayList<T>::pop_front() {
     if(!empty()) return tab[--last];
     return -1;
}

template<typename T>
int ArrayList<T>::front() {
     if(!empty()) return tab[last-1];
     return -1;
}

template<typename T>
int ArrayList<T>::back() {
     if(!empty()) return tab[0];
     return -1;
}

template<typename T>
void ArrayList<T>::clear() {
     int n;
     n = size();
     delete[] tab;
     last = 0;
     msize = n;
}

template<typename T>
void ArrayList<T>::reverse() {
     int n;
     n = size();
     for(int low = 0, high = n -1; low < high; low++, high--){
          std::swap(tab[low], tab[high]);
     }
}

template<typename T>
void ArrayList<T>::sort() {
    int n;
    n = size();
    for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n - 1; ++j) {
                if (tab[j] > tab[j+1]) {
                    std::swap(tab[j], tab[j+1]);
                }
            }
        }
}

template<typename T>
void ArrayList<T>::push_back(T x) {
     int i;
     for(i = size(); i > 0; i--) {
          tab[i] = tab[i-1];
     }
     tab[0] = x;
     last++;
}

template<typename T>
T ArrayList<T>::pop_back() {
     if(!empty()) {
          int n = size();
          int temp = tab[0];
          for(int i = 1; i < n; i++) {
               tab[i-1] = tab[i];
          }
          last--;
          return temp;
     } else {
          return -1;
     }
}

template<typename T>
int ArrayList<T>::find(T x) {
     int n = size();
     for(int i = 0; i < n; i++) {
          if(tab[i] == x) return i;
     }
     return -1;
}

template<typename T>
int ArrayList<T>::insert(int pos, T x) {
     if(!(pos < 1 || pos > size()+1)) {
          int i = 0;
          for(i = size()+1; i >= pos; i--) {
               tab[i] = tab[i-1];
          }
          tab[pos-1] = x;
          last++;
          return pos-1;
     } else return -1;
}

template<typename T>
void ArrayList<T>::display() {
     std::cout << "OUTPUT: " << std::endl;
     std::cout << "[";
     for(int i = 0; i < size(); i++) {
          std::cout << tab[i];
          if(i<size()-1){
               std::cout << ", ";
          }
     }
     std::cout << "]";
     std::cout << std::endl;
}

template<typename T>
int ArrayList<T>::erase(int x) {
     int n = size();
     if(x < 0) return -1;
     else if(x >= n-1) {
          pop_front();
          return x+1;
     } else {
          for(int i = x; i < n; i++) {
               tab[i] = tab[i+1];
          }
          last--;
          return x+1;
     }
}

template<typename T>
bool ArrayList<T>::full() {
     return last == msize;
}

template<typename T>
int ArrayList<T>::size() {
     return last;
}

template<typename T>
bool ArrayList<T>::empty() {
     return last == 0;
}

#endif