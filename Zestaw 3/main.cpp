#include <iostream>
#include <cctype>
#include <string>
#include "ArrayList.h"
#include "list"

using namespace std;

void help(){
     cout << "========================" << endl;
     cout << "F - push_front()" << endl;
     cout << "B - push_back()" << endl;
     cout << "S - sprawdz jaki jest rozmiar" << endl;
     cout << "N - pop_front" << endl;
     cout << "M - pop_back" << endl;
     cout << "P - wyswietl liste" << endl;
     cout << "E - sprawdz, czy lista jest pusta" << endl;
     cout << "C - znajdz index wskazanej cyfry" << endl;
     cout << "L - posortuj liste" << endl;
     cout << "R - odwroc liste" << endl;
     cout << "x - komenda clear" << endl;
     cout << "h - help" << endl;
     cout << "Przykladowa komenda: f 1 --> dodajemy cyfre do przodu" << endl;
}
int main() {

     int size;

     cout << "Podaj rozmiar tablicy: ";
     cin >> size;
     
     if(!system("clear"));

     ArrayList<int> list(size);
     string input;
     help();

     while(cin >> input) {
          char inputChar = toupper(input.at(0)); // case not sensitive
          switch(inputChar) {
               case 'F': // push_front
                    if(!list.full()) {
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodano element z przodu listy" << endl;
                         list.push_front(x);
                    } else {
                         cout << "Lista jest pelna!" << endl;
                    }
                    break;
               case 'B': // push_back
                    if(!list.full()) {
                         cin >> input;
                         int x = stoi(input);
                         cout << "Dodano element z tylu listy" << endl;
                         list.push_back(x);
                    } else {
                         cout << "Lista jest pelna!" << endl;
                    }
                    break;
               case 'S': // size
                    cout << "W liscie znajduje/a sie: " << list.size() << " element/y" << endl;
                    break;
               case 'N': // pop_front
                    if(!list.empty()) {
                         cout << "Wyciagamy element z przodu listy: " << list.pop_front() << endl;
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;
               case 'M': // pop_back
                    if(!list.empty()) {
                         cout << "Usuwamy z tylu listy element: " << list.pop_back() << endl;
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;
               case 'P':
                         list.display();
                    break;
               case 'L':
                    if(!list.empty()) {
                         cout << "Sortujemy liste!" << endl;
                         list.sort();
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;
               case 'R':
                    if(!list.empty()) {
                         cout << "Odwracamy liste!" << endl;
                         list.reverse();
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;
               case 'X':
                    bool checkList;
                    checkList = list.empty();
                    if(!checkList) {
                         list.clear();
                    } else {
                         cout << "Lista jest pusta! Nie mozna jej wyczyscic!" << endl;
                    }
                    break;
               case 'E':
                    bool check;
                    check = list.empty();
                    if(check){
                         cout << "Lista jest pusta!" << endl;
                    } else {
                         cout << "Lista nie jest pusta!" << endl;
                    }
                    break;
               case 'C':
                    if(!list.empty()) {
                         cin >> input;
                         int x = stoi(input);
                         int pos;
                         if((pos = list.find(x)) != -1) {
                              cout << "Znaleziono na pozycji: " << pos << endl;
                         } else {
                              cout << "Nie znaleziono!" << endl;
                         }                         
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
                    break;
               case 'Q':
                    if(!list.empty()) {
                         cout << "Tail listy: " << list.back() << endl;
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
               break;
               case 'W':
                    if(!list.empty()) {
                         cout << "Front listy: " << list.front() << endl;
                    } else {
                         cout << "Lista jest pusta!" << endl;
                    }
               break;
               case 'h':
                    help();
                    break;
               default:
                    break;
          }
     }

     return 0;
}