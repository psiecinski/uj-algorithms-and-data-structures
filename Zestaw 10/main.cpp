#include <iostream>
#include "bsttree.h"

using namespace std;

int main() {
    BinarySearchTree *bst = new BinarySearchTree();
    cout << "Dodaje nastepujace liczby: 33, 15" << endl;
    bst->insert(33);
    bst->insert(15);
    BSTNode *maximum = bst->find_max();
    cout << "Max: " << maximum->value << endl;
    BSTNode *minimum = bst->find_min();
    cout << "Min " << minimum->value << endl;
    cout << "Display: " << endl;
    bst->display();
    cout << "Usuwam 15" << endl;
    bst->remove(15);
    cout << "Display: " << endl;
    bst->display();
}