#ifndef BinarySearchTree_H
#define BinarySearchTree_H

using namespace std;

struct BSTNode {
    int value;
    BSTNode *right;
    BSTNode *left;
    BSTNode(int value) {
        this->value = value;
        this->right = NULL;
        this->left = NULL;
    }
};

class BinarySearchTree {
    
    private:
        BSTNode *root;
        void insert(BSTNode *root, int value);
        bool remove(BSTNode *parent, BSTNode *current, int value);
        BSTNode* nextLarger(BSTNode *node);
        BSTNode* search(int value);
        void display(BSTNode *node);

    public:
        void insert(int value);
        bool remove(int value);
        BSTNode* find_min();
        BSTNode* find_max();
        BSTNode* get_root();
        void display();

};

BSTNode* BinarySearchTree::search(int value) {
    BSTNode *current = root;
    while (current) {   
        if (current->value == value)
            break;
        
        if (value > current->value)
            current = current->right;
        else
            current = current->left;

    }
    return current;
}

BSTNode* BinarySearchTree::nextLarger(BSTNode *node) {
    BSTNode *nextLarger = node->right;
    while (nextLarger->left) {
        nextLarger = nextLarger->left;
    }

    return nextLarger;
}
void BinarySearchTree::insert(BSTNode *root, int value) {
    
    if (value > root->value) {
        if (!root->right)
            root->right = new BSTNode(value);
        else
            insert(root->right, value);
    }
    else {
        if (!root->left)
            root->left = new BSTNode(value);
        else
            insert(root->left, value);
    }
}
void BinarySearchTree::insert(int value) {
    
    if (!root) {
        root = new BSTNode(value);
    }
    else {
        this->insert(root, value);
    }

}

bool BinarySearchTree::remove(BSTNode *parent, BSTNode *current, int value) {
    if (!current) {
        return false;
    }
    if (current->value == value) {
        if (!current->left && !current->right)
        {
            if (parent->right == current) {
                parent->right = NULL;
            }
            else {
                parent->left = NULL;
            }
            delete current;
            current = NULL;
            return true;
        }
        else if (!current->left || !current->right) {
            BSTNode *child = current->left;
            if (!child) {
                child = current->right;
            }

            if (!parent) {
                this->root = child;
            }
            else {
                if (child->value > parent->value) {
                    parent->right = child;
                }
                else {
                    parent->left = child;
                }
            }
            delete current;
            current = NULL;
        }
        
        else {
            BSTNode *largerNode = nextLarger(current);
            current->value = largerNode->value;
            delete largerNode;
            largerNode = NULL;
        }
        return true;
    }

    if (value > current->value) {
        return remove(current, current->right, value);
    } else {
        return remove(current, current->left, value);
    }
}

void BinarySearchTree::display(BSTNode*root) {
    if (!root)
        return;
    display(root->left);
    cout<<root->value<<" ";
    display(root->right);
}

void BinarySearchTree::display() {
    if(!root)
        return;
    display(root);
    cout << endl;
}

BSTNode* BinarySearchTree::find_max() {
    BSTNode *max = root;
    while (max->right) {
        max = max->right;
    }
    return max;
}

BSTNode* BinarySearchTree::find_min() {
    BSTNode *min = root;
    while (min->left) {
        min = min->left;
    }
    return min;
}

bool BinarySearchTree::remove(int value) {
    return remove(NULL, root, value);
}
BSTNode* BinarySearchTree::get_root() {
    return root;
}

#endif