#include "avltree.h"
#include <fstream>
#include <iostream>


void help(){
     std::cout << "========================" << std::endl;
     std::cout << "F - search()" << std::endl;
     std::cout << "S - size()" << std::endl;
     std::cout << "I - insert()" << std::endl;
     std::cout << "J - preOrder()" << std::endl;
     std::cout << "K - inOrder()" << std::endl;
     std::cout << "L - postOrder()" << std::endl;
     std::cout << "C - remove()" << std::endl;
     std::cout << "Z - height()" << std::endl;
     std::cout << "V - visualize()" << std::endl;
     std::cout << "T - test()" << std::endl;
     std::cout << "B - printBalance()" << std::endl;
     std::cout << "H - help()" << std::endl;
     std::cout << "Q - quit()" << std::endl;
     std::cout << "Przykladowa komenda: i 12 --> dodajemy  cyfre '12'" << std::endl;
}
void test() {
    AVLTree tree;
    tree.insert(100);
    tree.insert(50);
    tree.insert(150);
    tree.insert(25);
    tree.insert(75);
    tree.insert(125);
    tree.insert(175);
    tree.insert(65);
    tree.insert(85);
    tree.inOrder();
    std::cout << std::endl;
    std::cout << "Wysokosc: " << tree.getHeight() << std::endl;
    std::cout << "Rozmiar: " << tree.size() << std::endl;
    std::cout << std::endl;
    tree.visualize();
    tree.printBalance();

}
int main() {

     AVLTree tree;
     int number;
     std::string input;
     help();
     while(std::cin >> input ) {
        char inputChar = toupper(input.at(0)); // case not sensitive
        if(inputChar == 'F') {
            if(tree.size()>0){

                    std::cin >> number;
                    if(tree.search(number) == nullptr) {
                        std::cout << "Nie ma" << std::endl;
                    }
                    else {
                        std::cout << "Jest" << std::endl;
                    }
                    
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }

            } else if(inputChar == 'S') {
                    if(tree.size()>0) {
                        std::cout << "Rozmiar: " << tree.size() << std::endl;
                    } else {
                        std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;

                    }

            } else if(inputChar == 'I') {

                    std::cin >> number;
                    tree.insert(number);

            } else if(inputChar == 'C') {

                    if(tree.size()>0) {
                        std::cin >> number;
                        tree.remove(number);
                    }else {
                        std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                    }


            } else if(inputChar == 'Z') {
                if(tree.size()>0) {
                    std::cout << "Wysokosc: " << tree.getHeight() << std::endl;
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }

            } else if(inputChar == 'J') {

                if(tree.size()>0) {
                    std::cout << "=========" << std::endl;
                    std::cout << "preorder" << std::endl;
                    std::cout << "=========" << std::endl;
                    tree.preOrder();
                    std::cout << std::endl;
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }

            } else if(inputChar == 'K') {

                if(tree.size()>0) {
                    std::cout << "=========" << std::endl;
                    std::cout << "inorder" << std::endl;
                    std::cout << "=========" << std::endl;
                    tree.inOrder();
                    std::cout << std::endl;
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }

            }else if(inputChar == 'L') {

                if(tree.size()>0) {
                    std::cout << "=========" << std::endl;
                    std::cout << "postorder" << std::endl;
                    std::cout << "=========" << std::endl;
                    tree.postOrder();
                    std::cout << std::endl;
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }
                
            }
            else if(inputChar == 'B') {

                if(tree.size()>0) {
                    std::cout << "=========" << std::endl;
                    std::cout << "Balance" << std::endl;
                    std::cout << "=========" << std::endl;
                    tree.printBalance();
                    std::cout << std::endl;
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }
                
            }else if(inputChar == 'E') {

                if(tree.size()>0) {
                    std::cout << "=========" << std::endl;
                    std::cout << "is Height Balanced?" << std::endl;
                    std::cout << "=========" << std::endl;
                    tree.isHeightBalanced()? std::cout << "Yes": std::cout << "No";
                    std::cout << std::endl;
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }
                
            } else if(inputChar == 'V') {
                if(tree.size()>0) {
                    std::cout << "==============" << std::endl;
                    std::cout << "visualize tree" << std::endl;
                    std::cout << "==============" << std::endl;
                    tree.visualize(); 
                } else {
                    std::cout << "Nie ma jeszcze zadnych elementow!" << std::endl;
                }

            } else if(inputChar == 'T') {
                    std::cout << "==============" << std::endl;
                    std::cout << "TEST" << std::endl;
                    std::cout << "==============" << std::endl;
                    test();
            } else if(inputChar == 'H') {
                    help();
            } else if(inputChar == 'Q') {
                    std::cout << "==============" << std::endl;
                    std::cout << "QUIT" << std::endl;
                    std::cout << "==============" << std::endl;
                break;
            }
     }
}

