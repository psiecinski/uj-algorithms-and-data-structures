#ifndef AVLTree_H
#define AVLTree_H
#include <iostream>

struct AVLNode {
     int key;
     AVLNode* left;
     AVLNode* right;
     int height;
     AVLNode(int key) {
          this->key = key;
          this->left = nullptr;
          this->right = nullptr;
          this->height = 1;
     }
     ~AVLNode() {
          delete left;
          delete right;
     }
};

class AVLTree {
private:
     AVLNode* root;
     int nodesNumber;

     int getHeight(AVLNode* node) {
          if(node == nullptr) {
              return 0;
          }
          return node->height;
     }

     AVLNode *search(AVLNode *node, int x) {
        if (node == nullptr)
            return nullptr;
        if(x == node->key)
            return node;
        if(x < node->key)
            return search(node->left, x);
        else
            return search(node->right, x);
    }

     int getBalance(AVLNode *node) {  
          if (node == nullptr)  
               return 0;  
          return getHeight(node->left) - getHeight(node->right);  
     }  

     AVLNode* rotateRight(AVLNode* node) {
          AVLNode* nodeOnLeft = node->left;
          AVLNode* temp = nodeOnLeft->right;
          nodeOnLeft->right = node;
          node->left = temp;
          node->height = std::max(getHeight(node->left), getHeight(node->right)) + 1;
          nodeOnLeft->height = std::max(getHeight(nodeOnLeft->left), getHeight(nodeOnLeft->right)) + 1;
          return nodeOnLeft;
     }

     AVLNode* rotateLeft(AVLNode* node) {
          AVLNode* nodeOnRight = node->right;
          AVLNode* temp = nodeOnRight->left;
          nodeOnRight->left = node;
          node->right = temp;
          node->height = std::max(getHeight(node->left), getHeight(node->right)) + 1;
          nodeOnRight->height = std::max(getHeight(nodeOnRight->left), getHeight(nodeOnRight->right)) + 1;
          return nodeOnRight;
     }

     AVLNode* insert(AVLNode* node, int key)  {  
          if (node == nullptr) {
               nodesNumber++;
               return new AVLNode(key);
          } 
     
          if (key < node->key)  node->left = insert(node->left, key);  
          else if (key > node->key)  node->right = insert(node->right, key);  
          else return node;  
     
          node->height = 1 + std::max(getHeight(node->left),  getHeight(node->right));  
          
          int balance = getBalance(node); 

          if (balance > 1 && key < node->left->key)  
               return rotateRight(node);  
          
          if (balance < -1 && key > node->right->key)  
               return rotateLeft(node);  
          if (balance > 1 && key > node->left->key) {  
               node->left = rotateLeft(node->left);  
               return rotateRight(node);  
          }  
          
          if (balance < -1 && key < node->right->key) {  
               node->right = rotateRight(node->right);  
               return rotateLeft(node);  
          }  
          return node;  
     }  

     void preOrder(AVLNode* node) {
          if (node == nullptr) {
               return;
          }
          std::cout << node->key << " ";
          preOrder(node->left);
          preOrder(node->right);
     }
     void inOrder(AVLNode* node) {
          if (node == nullptr) {
               return;
          }
          inOrder(node->left);
          std::cout << node->key << " ";
          inOrder(node->right);
     }
     void postOrder(AVLNode* node) {
          if (node == nullptr) {
               return;
          }
          postOrder(node->left);
          postOrder(node->right);
          std::cout << node->key << " ";
     }
     void printBalance(AVLNode* node) {
          if (node != nullptr) {
               printBalance(node->left);
               int balance = getBalance(node);  
               std::cout << balance << " ";
               printBalance(node->right);
          }
     }
     int isHeightBalanced(AVLNode* root, bool& isBalanced) {
     if (root == nullptr || !isBalanced)
          return 0;
          
          int left_height = isHeightBalanced(root->left, isBalanced);
          int right_height = isHeightBalanced(root->right, isBalanced);
     
          if (std::abs(left_height - right_height) > 1) {
          isBalanced = false;
          }
          return std::max(left_height, right_height) + 1;
     }

     AVLNode* findMin(AVLNode* node) {
          if(node == nullptr) return nullptr;
          else if(node->left == nullptr) return node;
          else return findMin(node->left);
     }
    
     AVLNode* findMax(AVLNode* node) {
          if(node == nullptr)
               return nullptr;
          else if(node->right == nullptr)
               return node;
          else
               return findMax(node->right);
     }
     void visualize(const std::string& prefix, AVLNode* node, bool isLeft) {
          if( node != nullptr ){
               std::cout << prefix;
               std::cout << (isLeft ? "├──" : "└──" );
               int balance = getBalance(node);  
               std::cout << node->key << " (" << balance << ") " << std::endl;
               visualize( prefix + (isLeft ? "│   " : "    "), node->left, true);
               visualize( prefix + (isLeft ? "│   " : "    "), node->right, false);
          }
     }
     AVLNode* remove(AVLNode* root, int x)  {  

          if (root == nullptr)  
               return root;  

          if ( x < root->key )  
               root->left = remove(root->left, x);  
          
          else if( x > root->key )  {
               root->right = remove(root->right, x);  
          }
          
          else {  
               if( (root->left == nullptr) || (root->right == nullptr) )  {  
                    AVLNode *temp = root->left ?  root->left :  root->right;  
                    if (temp == nullptr)  {  
                         temp = root;  
                         root = nullptr;  
                    }  
                    else {
                         *root = *temp;
                    }
                    nodesNumber-=1;
                    free(temp);  
               }  
               else {  
                    AVLNode* temp = findMin(root->right);  
                    root->key = temp->key;  
                    root->right = remove(root->right, temp->key);  
               }  
          }  
     
          if (root == nullptr) {
               return root;  
          } 
          
          root->height = 1 + std::max(getHeight(root->left), getHeight(root->right));  
          
          int balance = getBalance(root);  
          
          if (balance > 1 && getBalance(root->left) >= 0) {
               return rotateRight(root);  
          } 
          
          if (balance> 1 && getBalance(root->left) < 0)  {  
               root->left = rotateLeft(root->left);  
               return rotateRight(root);  
          }  
          
          if (balance < -1 &&  getBalance(root->right) <= 0)  
               return rotateLeft(root);  
          
          if (balance < -1 &&  getBalance(root->right) > 0) {  
               root->right = rotateRight(root->right);  
               return rotateLeft(root);  
          }  
          return root;  
     }  

public:
     AVLTree(): root(nullptr), nodesNumber(0) { }
     ~AVLTree() {
          delete root;
     }
     int size() { //zwraca liczbę kluczy w drzewie
          return nodesNumber;
     }
     void printBalance() {
          printBalance(root);
     }
     AVLNode* search(int s) {
          return search(root, s);
     }
     AVLNode* findMin() {
          return findMin(root); 
     }
     AVLNode* findMax() {
          return findMax(root); 
     }
     void visualize(){
          visualize("", root, false);    
     }
     void insert(int x) {
          root = insert(root, x);
     }
     void preOrder() {
          preOrder(root); //przejście wzdłużne
     }
     void inOrder() {
          inOrder(root); //przejście poprzeczne
     }
     void postOrder() {
          postOrder(root); //przejście wsteczne
     }
     bool isHeightBalanced(){
          bool isBalanced = true;
          isHeightBalanced(root, isBalanced);
          return isBalanced;
     }
     int getHeight() {
          return getHeight(root);
     }
     void remove(int x) {
          root = remove(root, x);
     }
};

#endif